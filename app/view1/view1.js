'use strict';

angular.module('myApp.view1', ['ngRoute'])

.config(['$routeProvider', function($routeProvider) {
	$routeProvider.when('/view1', {
		templateUrl: 'view1/view1.html',
		controller: 'View1Ctrl'
	});
}])


.controller('View1Ctrl', ["$scope", function($scope) {
	$scope.pokemons = [
	{
		name: 'Мьюту',
		typeOf: 'Специальный', 
		type: 'Психический', 
		view: 'Генетический Покемон',
		growth: 2,
		weight: 121.99, 
		url: 'https://cyberlord.ru/media/res/3/6/4/364.opqgmc.620.jpg'
	},
	{
		name: 'Драгонайт',
		typeOf: 'Специальный', 
		type: 'Драконий, летающий', 
		view: 'Дракон',
		growth: 2.2,
		weight: 210, 
		url: 'https://cyberlord.ru/media/res/3/6/5/365.opqgmc.620.jpg'
	},
	{
		name: 'Мью',
		typeOf: 'Специальный', 
		type: 'Психический', 
		view: 'Новый вид',
		growth: 0.4,
		weight: 4, 
		url: 'https://cyberlord.ru/media/res/3/6/6/366.opqgmc.620.jpg'
	},
	{
		name: 'Молтрес',
		typeOf: 'Специальный', 
		type: 'Огненный, летающий', 
		view: 'Пламя',
		growth: 2,
		weight: 60, 
		url: 'https://cyberlord.ru/media/res/3/6/7/367.opqgmc.620.jpg'
	},
	{
		name: 'Запдос',
		typeOf: 'Специальный', 
		type: 'Психический', 
		view: 'Генетический Покемон',
		growth: 2,
		weight: 121.99, 
		url: 'https://cyberlord.ru/media/res/3/6/8/368.opqgmc.620.jpg'
	},
	{
		name: 'Снорлакс',
		typeOf: 'Специальный', 
		type: 'Электрический, летающий', 
		view: 'Молния',
		growth: 1.6,
		weight: 52.6, 
		url: 'https://cyberlord.ru/media/res/3/6/9/369.opqgmc.620.jpg'
	},
	{
		name: 'Арканайн',
		typeOf: 'Специальный', 
		type: 'Огненный', 
		view: 'Легендарный',
		growth: 1.9,
		weight: 155, 
		url: 'https://cyberlord.ru/media/res/3/7/0/370.opqgmc.620.jpg'
	},
	{
		name: 'Лапрас',
		typeOf: 'Специальный', 
		type: 'Водный, ледяной', 
		view: 'Транспорт',
		growth: 2.5,
		weight: 220, 
		url: 'https://cyberlord.ru/media/res/3/7/1/371.opqgmc.620.jpg'
	},
	{
		name: 'Артикуно',
		typeOf: 'Специальный', 
		type: 'Ледяной, летающий', 
		view: 'Мороз',
		growth: 1.7,
		weight: 55.4, 
		url: 'https://cyberlord.ru/media/res/3/7/2/372.opqgmc.620.jpg'
	},
	{
		name: 'Экзекутор',
		typeOf: 'Специальный', 
		type: 'Травяной, психический', 
		view: 'Генетический Покемон',
		growth: 2,
		weight: 120, 
		url: 'https://cyberlord.ru/media/res/3/7/3/373.opqgmc.620.jpg'
	},
	{
		name: 'Иви',
		typeOf: 'Нормальный', 
		type: '-', 
		view: 'Эволюционный Покемон',
		growth: 1.3,
		weight: 6.3, 
		url: 'https://2ch.hk/pok/src/68125/14961685736340.jpg'
	},
	{
		name: 'Мачоп',
		typeOf: 'Нормальный', 
		type: 'Боевой', 
		view: 'Cупермощный Покемон',
		growth: 0.79,
		weight: 19.5, 
		url: 'https://vignette4.wikia.nocookie.net/pokemon/images/0/09/66-Machop.jpg/revision/latest?cb=20170416090942&path-prefix=ru'
	},
	{
		name: 'Нидоран',
		typeOf: 'Нормальный', 
		type: 'Ядовитый', 
		view: 'Покемон-Иглы Яда',
		growth: 0.4,
		weight: 7, 
		url: 'http://pokemongocommunity.ru/img/big/nidoran-f.png'
	},
	{
		name: 'Стиликс',
		typeOf: 'Нормальный', 
		type: 'Стальной, земляной', 
		view: 'Покемон-Железная Змея',
		growth: 9.19,
		weight: 399.9, 
		url: 'http://pokemongolife.ru/p/Onix.png'
	},
	{
		name: 'Двеббл',
		typeOf: 'Нормальный', 
		type: 'Насекомое, каменный', 
		view: 'Покемон с каменной раковиной',
		growth: 0.3,
		weight: 14.5, 
		url: 'http://poke-universe.ru/pic/dex/gen5/anime/557.png'
	},
	];
	$scope.predicate = '-age';
	$scope.type = '';


	$scope.alert_name = function(name) {
		alert(name);
	};
}]);

